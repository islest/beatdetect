// Main.java

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import context.*;
import audio.*;
import visualiser.*;
import fft.*;
import utilities.*;

public class Main
{
    public static void main(String[] args)
    {
        final Context context = new Context();
        final AudioInput input = new AudioInput(args[0]);
        final AudioOutput output = new AudioOutput(input.getFormat());
        final FFT fftLeft = new FFT();
        final FFT fftRight = new FFT();
        final GUI window = new GUI(context.NUM_SAMPLES_PER_FRAME, 480); 

        Thread audio = new Thread(new Runnable()
        {
            @Override
            public void run()
            {               
                output.open();
                System.out.println(input.getFormat());
                
              
                AudioSampleFrame frame = input.getNextFrame(); 

		        while (!input.playbackFinished())
                { 
                    fftLeft.calculateFFT(frame.getLeftChannel());
                    fftRight.calculateFFT(frame.getRightChannel());
                    window.update(fftLeft.getMostRecentTransform(), fftRight.getMostRecentTransform());
                    output.writeToOutput(frame);
                    frame = input.getNextFrame(); 
                }
                output.close();
            }
        });

        Thread graphics = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                long lastLoopTime = System.nanoTime();
                final int TARGET_FPS = 100;
                final long OPTIMAL_TIME = 100000000 / TARGET_FPS;
                int lastFpsTime = 0;
                int fps = 0;

                while (!input.playbackFinished())
                {
                    long now = System.nanoTime();
                    long updateLength = now - lastLoopTime;
                    lastLoopTime = now;
                    double delta = updateLength / (double) OPTIMAL_TIME;

                    lastFpsTime += updateLength;
                    fps++;

                    if (lastFpsTime >= 100000000)
                    {
                        System.out.println("FPS: " + fps);
                        lastFpsTime = 0;
                        fps = 0;
                    }
                    
                    window.render();

                    try
                    {
                        Thread.sleep((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) /
                                      100000000);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        });
      
        audio.start();
        graphics.start();        
    }
}


