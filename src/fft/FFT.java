package fft;

// FFT.java
// It gets 'updated' with a byte array, which it converts to doubles
// in front of their eyes, then it simply runs an FFT, and outputs that FFT


import utilities.*;
import context.*;
import audio.*;

public class FFT
{
    public FFT()
    {
        if (!this.runTests())
        {
            System.out.println("Error occurred testing FFT");
            System.exit(1);
        }
        this.mostRecentTransform = null;   
    }

    // This I assume is where thee FFT Occurs
    public void calculateFFT(AudioSampleFrame frame)
    {
        this.mostRecentTransform = this.fft(frame.asComplexArray());
    }

    public Complex[] getMostRecentTransform()
    {
        return this.mostRecentTransform;
    }


    // For now we derive this from princeton! Although we may optimise this in future.
    private Complex[] fft(Complex[] subject)
    {
        int length = subject.length;
        if (length == 1) { return new Complex[] { subject[0] }; }

        // Only works on powers of 2, otherwise return null
        if (length % 2 != 0) { return null; }

        // Divide here!
        Complex[] even = new Complex[length/2];
        Complex[] odd = new Complex[length/2];
        
        for (int i = 0; i < length/2; i++)
        {
            even[i] = subject[2*i];
            odd[i]  = subject[2*i+1];
        }
        // Recursive calculation of FFT!
        Complex[] evenFFT = this.fft(even);
        Complex[] oddFFT = this.fft(odd);

        // Combine Results!
        Complex[] result = new Complex[length];

        for (int i = 0; i < length/2; i++)
        {
            double power = -2 * i * Math.PI/length;
            Complex rootOfUnity = new Complex(Math.cos(power), Math.sin(power));
                           
            result[i] = evenFFT[i].add(rootOfUnity.multiply(oddFFT[i]));
            result[i+length/2] = evenFFT[i].subtract(rootOfUnity.multiply(oddFFT[i]));; 
        }
        return result;
    }
  
    private boolean runTests()
    {
        Complex[] testCase = new Complex[8];
        for (int i = 0; i < testCase.length; i++)
        {
            testCase[i] = new Complex(i+1, 0);
        }

        Complex[] result = this.fft(testCase);
        if ((int) result[0].real() != 36)
        {
            System.out.println(result[0].real());

            return false;
        }

        for (int i = 0; i < result.length; i++)
        {
            System.out.println(i + ": " + result[i].real() + " + " + result[i].imag() + "i");
        }   
        return true;
    }

    private Complex[] mostRecentTransform;
}
