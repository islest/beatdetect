package visualiser;

// FFTPane.java

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.nio.ByteBuffer;

import context.*;
import audio.*;
import utilities.*;
import fft.*;

public class FFTPane extends JPanel
{

    public FFTPane()
    {
        this.context = new Context();
        this.width = context.NUM_SAMPLES_PER_FRAME;
        this.height = 480;
        this.image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);
        this.mostRecentFFTLeft = new Complex[context.NUM_SAMPLES_PER_FRAME];
        this.mostRecentFFTRight = new Complex[context.NUM_SAMPLES_PER_FRAME];
        for (int i = 0; i < context.NUM_SAMPLES_PER_FRAME; i++)
        {
            this.mostRecentFFTLeft[i] = new Complex(0,0);
            this.mostRecentFFTRight[i] = new Complex(0,0);
        }
    }

    public void update(Complex[] fftArrayLeft, Complex[] fftArrayRight)
    {
        this.mostRecentFFTLeft = fftArrayLeft;
        this.mostRecentFFTRight = fftArrayRight;
    }

    @Override
    public void paintComponent(Graphics g)
    {
     
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Graphics2D imageGraphics = (Graphics2D) image.getGraphics(); 
        imageGraphics.setColor(Color.BLACK);
        imageGraphics.fillRect(0,0, this.width, this.height); 
        imageGraphics.setColor(Color.GREEN);
        
        for (int i = 0; i < this.width/2; i++)
        {
            imageGraphics.setColor(new Color(150, (i*150)/(this.width/2), (i*150)/(this.width/2)));
            int startPoint = i * 2;
            double magnitudeLeft = this.mostRecentFFTLeft[i].modulus();
            double magnitudeRight = this.mostRecentFFTRight[i].modulus();
            double amplitudeLeft = ((magnitudeLeft) / context.NUM_SAMPLES_PER_FRAME) * 2;
            double amplitudeRight = ((magnitudeRight) / context.NUM_SAMPLES_PER_FRAME) * 2; 
            
        
            amplitudeLeft =  10 * Math.log10(amplitudeLeft);
            amplitudeRight =  10 * Math.log10(amplitudeRight);
        
            amplitudeLeft *= 4;
            amplitudeRight *= 4;
            
 




            imageGraphics.fillRect(startPoint, this.height/2-(int)amplitudeLeft, 2, (int)amplitudeLeft);
            imageGraphics.fillRect(startPoint, this.height/2, 2, (int)amplitudeRight);           
        }

        
        g2d.drawImage(this.image, 0,0,this.width,this.height,null,null);  
    }

    private Complex[] mostRecentFFTLeft;
    private Complex[] mostRecentFFTRight; 
    private BufferedImage image;
    private int width;
    private int height;
    private final Context context;
}
