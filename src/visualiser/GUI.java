package visualiser;

// GUI.java

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

import audio.*;
import utilities.*;
import fft.*;
import context.*;


public class GUI 
{
    public GUI(int width, int height)
    {
        this.frame = new JFrame("FFT Visualiser");
        this.size = new Dimension(width, height);
        this.frame.setPreferredSize(this.size); 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

        this.panel = new FFTPane();
        this.frame.add(this.panel);

        this.frame.pack();
        this.frame.setVisible(true);
    }

    public void update(Complex[] fftArrayLeft, Complex[] fftArrayRight)
    {
        this.panel.update(fftArrayLeft, fftArrayRight);
    } 

    public void render() {
        this.panel.repaint();   
    }

    private JFrame frame;
    private FFTPane panel;
    private Dimension size;  
}
