package utilities;

/**
* Complex.java
*  A class for storing complex numbers and dealing with complex number arithmetic - appropriated
*  from University of Princeton's Complex class located here:
*      http://introcs.cs.princeton.edu/java/97data/Complex.java.html
* 
**/

public class Complex {

    public Complex(double real, double imag)
    {
        this.re = real;
        this.im = imag;
    }

    public Complex add(Complex b)
    {
        Complex a = this;
        return new Complex(a.re + b.re, a.im + b.im);
    }

    public Complex subtract(Complex b)
    {
        Complex a = this;
        return new Complex(a.re - b.re, a.im - b.im);
    }

    public Complex multiply(Complex b)
    {
        Complex a = this;
        double newReal = a.re * b.re - a.im * b.im;
        double newImag = a.re * b.im + a.im * b.re;
        return new Complex(newReal, newImag);
    }

    public Complex multiply(double scalar)
    {
        Complex a = this;
        return new Complex(a.re * scalar, a.im * scalar);
    }

    public Complex reciprocal() 
    {
        Complex a = this;
        double scale = a.re*a.re + a.im*a.im;
        return new Complex(a.re/scale, -a.im/scale);
    }

    public Complex divide(Complex b)
    {
        Complex a = this;
        return a.multiply(b.reciprocal());
    }

    public Complex conjugate()
    {
        Complex a = this;
        return new Complex(a.re, -a.im);
    }

    public double modulus()
    {
        double realSquared = this.re * this.re;
        double imagSquared = this.im * this.im;
        
        return Math.sqrt(realSquared + imagSquared);
    }

    public double real() { return this.re; }
    public double imag() { return this.im; }

    public String toString()
    {
        if (im == 0) return Double.toString(re);
        if (re == 0) return Double.toString(im) + "i";
        if (im < 0) return Double.toString(re) + " - " + Double.toString(-im) + "i";
        return Double.toString(re) + " + " + Double.toString(im) + "i";
    }

    private final double re;
    private final double im;
}
