package audio;

/**
 * AudioOutput.java
 *  Sane wrapper for javax.sound.sampled library which creates a SourceDataLine object
 *  used to write audio samples to the speaker for playback - AudioSampleFrame objects
 *  are passed in once they've been constructed by AudioInput and these are used to extract
 *  our byte array payload for speaker playback.
 *
 **/

import javax.sound.sampled.*;

public class AudioOutput
{
    /**
    * AudioOutput(f):
    *     CONSTRUCTOR - instantiates a SourceDataLine object for use outputting
    *     AudioSampleFrame objects to the speaker.
    * 
    *     @param f AudioFormat object specifying the format of the file being played.
    **/
    public AudioOutput(AudioFormat f)
    {   
        try
        {
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, f);
            this.destinationLine = (SourceDataLine) AudioSystem.getLine(info);
            this.format = f;    
            this.destinationLine.open(this.format);
            this.destinationLine.start();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
    * open():
    *     Marks class as ready to output - then opens the SourceDataLine for
    *     receiving input and starts it.
    *
    *     @pre The line must be closed - close() must have been called, or the object
    *          has just been instantiated.     
    *     @post writeToOutput() can be called.
    **/
    public void open()
    {
        if (this.destinationLine.isOpen()) { return; }
        try
        {
            this.destinationLine.open(this.format);
            this.destinationLine.start();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
    * finishPlayback():
    *     Marks class as having finished playing - drains and closes SourceDataLine object,
    *     preventing it from throwing stuff to the speaker in the future.
    *     
    *     @pre open() must have been called.
    *     @post writeToOutput() will have no effect, playback is considered finished and
    *     our SourceDataLine object is closed.
    **/
    public void close()
    { 
        this.destinationLine.drain();
        this.destinationLine.close();
    }

    /**
    * writeToOutput(frame):
    *     Takes data from an AudioSampleFrame object and throws it at the speakers, hoping
    *     desperately for playback in a cruel exception-filled world...
    *     
    *     @param frame AudioSampleFrame object, usually generated by an AudioInput object then passed here.
    *     @pre startPlayback() must have been called.
    *     @post Some audio will play, hopefully.
    **/
    public void writeToOutput(AudioSampleFrame frame)
    { 
        if (!this.destinationLine.isOpen()) { return; }

        int numBytes = frame.getNumBytesUsed();
        try
        {
            if (numBytes >= 0)
            {
                this.destinationLine.write(frame.asByteArray(), 0, numBytes);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    // DATA MEMBERS
    private AudioFormat format;
    private SourceDataLine destinationLine;
}
