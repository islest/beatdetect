package audio;

/**
 * AudioInput.java
 *  Sane wrapper for javax.sound.sampled library which creates an AudioInputStream
 *  object from a .wav file and outputs it in byte arrays of a size defined by 
 *  the user - these arrays are wrapped up in our own AudioSampleFrame object
 *  so we can easily convert them to other data formats.
 *
 **/

import context.*;
import utilities.*;
import fft.*;

import javax.sound.sampled.*;
import java.io.File;

public class AudioInput
{
    /**
    * AudioInput(filename, bufferSize):
    *     CONSTRUCTOR - Takes in a file and buffer size integer, creates an audio input stream from the file
    *     and readies the stream for playback.
    *
    *     @param filename Name of sound file including .wav extension
    **/
    public AudioInput(String filename)
    {
        this.context = new Context();
        try {
            this.soundFile = new File(filename);
            this.inputStream = AudioSystem.getAudioInputStream(this.soundFile);
            this.inputFormat = this.inputStream.getFormat();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        this.bufferSize = this.context.NUM_SAMPLES_PER_FRAME * this.context.NUM_BYTES_PER_SAMPLE;
        this.numBytesRead = 0;
    }

    /**
    * getNextFrame():
    *     Reads in the next amount of bytes from inputStream as specified by bufferSize data member -
    *     wraps them in an AudioSampleFrame object.     
    *
    *     @pre startPlayback() has been called.
    *     @post Next set of bytes will be ready for retrieval, or playback will have finished.
    *     @return AudioSampleFrame object or null if playback has finished.
    **/
    public AudioSampleFrame getNextFrame()
    {
        if (this.numBytesRead == -1) { return null; }
        
        byte[] sampleFrame = new byte[this.bufferSize];
        try
        {
            this.numBytesRead = this.inputStream.read(sampleFrame, 0, sampleFrame.length);
        } catch (Exception e)
        {
            System.out.println("EXCEPTION!");
            e.printStackTrace();
        }
        AudioSampleFrame frame = new AudioSampleFrame(sampleFrame, this.bufferSize, this.numBytesRead);
        
        return frame;
    }
 
    /**
    * playbackFinished():
    *     @return Whether or not playback has finished.
    **/
    public boolean playbackFinished() { return this.numBytesRead == -1; }

    /**
    * getFormat():
    *     @return Format of soundfile, as AudioFormat object.
    **/
    public AudioFormat getFormat()    { return this.inputFormat; }

    // DATA MEMBERS
    private File soundFile;
    private AudioInputStream inputStream;
    private AudioFormat inputFormat;
    private int numBytesRead;
    private int bufferSize;
    private boolean finished;

    private final Context context;
}
