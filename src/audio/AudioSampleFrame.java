package audio;

/**
* AudioSampleFrame.java
*  Sane wrapper for byte arrays containing signal data - used as a payload and contains handy conversion
*  methods for use with FFT, and other algorithms, and supports splitting a stereo signal into two
*  mono signals.
*
**/

import java.nio.*;
import utilities.*;

public class AudioSampleFrame 
{   
    public AudioSampleFrame(byte[] frame, int bufferSize, int bytesRead)
    { 
        this.frame = frame;
        this.bufferSize = bufferSize;
        this.numBytesUsed = bytesRead;
    }

    public double[] asDoubleArray()
    {
        ByteBuffer b = ByteBuffer.wrap(this.frame);
        b.order(ByteOrder.LITTLE_ENDIAN);
        double[] result = new double[this.bufferSize/4];

        for (int i = 0; i < result.length; i++)
        {
	    result[i] = b.getShort();	
	}

        return result;   
    }

    public Complex[] asComplexArray()
    {
        double[] frameAsDouble = this.asDoubleArray();
        Complex[] result = new Complex[frameAsDouble.length];
        for (int i = 0; i < result.length; i++)
        {
            result[i] = new Complex(frameAsDouble[i], 0);
        }
        return result;
    }

    public AudioSampleFrame getLeftChannel()
    {
        byte[] leftChannel = new byte[this.bufferSize/2];
        byte[] rightChannel = new byte[this.bufferSize/2];
        int leftChannelIndex = 0;
        int rightChannelIndex = 0;

        for (int i = 0; i < this.bufferSize; i += 4)
        {
            leftChannel[leftChannelIndex++] = this.frame[i];
            leftChannel[leftChannelIndex++] = this.frame[i+1];
            rightChannel[rightChannelIndex++] = this.frame[i+2];
            rightChannel[rightChannelIndex++] = this.frame[i+3];
        }
        return new AudioSampleFrame(leftChannel, bufferSize/2, numBytesUsed/2);
    }

    public AudioSampleFrame getRightChannel()
    {
        byte[] leftChannel = new byte[this.bufferSize/2];
        byte[] rightChannel = new byte[this.bufferSize/2];
        int leftChannelIndex = 0;
        int rightChannelIndex = 0;

        for (int i = 0; i < this.bufferSize; i += 4)
        {
            leftChannel[leftChannelIndex++] = this.frame[i];
            leftChannel[leftChannelIndex++] = this.frame[i+1];
            rightChannel[rightChannelIndex++] = this.frame[i+2];
            rightChannel[rightChannelIndex++] = this.frame[i+3];
        }
        return new AudioSampleFrame(rightChannel, bufferSize/2, numBytesUsed/2);
    }

    public byte[] asByteArray() { return this.frame; }
    public int getSize() { return this.bufferSize; }
    public int getNumBytesUsed() { return this.numBytesUsed; }

    private final byte[] frame;
    private final int bufferSize; 
    private final int numBytesUsed;
}
