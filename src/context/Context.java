package context;

// Context.java
// Contains data related to buffer sizes, byte sizes, screen width, screen height, shit like that

public class Context
{
    // Audio Frame Data
    public final int NUM_SAMPLES_PER_FRAME = 1024;
    public final int NUM_BYTES_PER_SAMPLE = 8;
    public final int NUM_SAMPLES_NO_CONJUGATE = NUM_SAMPLES_PER_FRAME / 2;
}

