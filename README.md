Beat Detect
===========

A Java-based music visualiser coded for a uni assignment. Uses FFT to perform frequency analysis then draws the resulting visualisation in a window using java swing.

Coded in Java 7.

## Usage ##
```

cd src/
java Main <absolute .wav file path>

```